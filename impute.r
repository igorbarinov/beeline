library(Amelia)
library(parallel)
ncpus = detectCores(all.tests = FALSE, logical = TRUE)
setwd('/Users/bis/code/beeline')

train = read.csv('train.csv')
test = read.csv('test.csv')
output_file_stem = 'train_and_test_imp'
cols = colnames( train )
noms = c('x0','x1','x2','x3','x4','x5','x9','x10','x11','x12','x14','x15','x16','x17','x18','x19','x20','x21','x22')
test['ID'] <- NULL
test['y'] = NA
df = rbind( train, test )


a.out = amelia(df, m = 2, idvars = c('y'), noms = noms, parallel = 'multicore', ncpus = ncpus)
write.amelia(a.out, file.stem = output_file_stem)

