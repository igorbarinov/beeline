
# coding: utf-8

# In[9]:

import pandas as pd
import numpy as np
import sklearn 
from sklearn.naive_bayes import *
from sklearn.svm import SVC


# In[10]:

train = pd.read_csv('train.csv').fillna(0)

train_data = train
expected = train['y']
# train_data = train.drop(['x0','x1','x2','x3','x4','x5','x9','x10','x11','x12','x14','x15','x16','x17','x18','x19','x20','x21','x22','y'],axis=1)
#print train_data.size
train_data = train_data.drop(['y'],axis=1)

from sklearn import preprocessing
le = preprocessing.LabelEncoder()

l = ['x0','x1','x2','x3','x4','x5','x9','x10','x11','x12','x14','x15','x16','x17','x18','x19','x20','x21','x22']
for i in range(len(l)):
    le.fit(train_data[l[i]])
    train_data[l[i]] = le.transform(train_data[l[i]])

#train_data = train_data.values


# In[11]:

test = pd.read_csv('test.csv').fillna(0)

#test.select_dtypes(include=['object']).info()
#test_data = train.drop(['x0','x1','x2','x3','x4','x5','x9','x10','x11','x12','x14','x15','x16','x17','x18','x19','x20','x21','x22','y'],axis=1).values
test_data = test
l = ['x0','x1','x2','x3','x4','x5','x9','x10','x11','x12','x14','x15','x16','x17','x18','x19','x20','x21','x22']
for i in range(len(l)):
    le.fit(test_data[l[i]])
    test_data[l[i]] = le.transform(test_data[l[i]])
test_data = test_data.drop(['ID'],axis=1)
#test_data
#test_data = test_data.values
   


# In[12]:

model = BernoulliNB()
model.fit(train_data, expected)
print model
predicted = model.predict(test_data)

#print(metrics.classification_report(expected, predicted))
#print(metrics.confusion_matrix(expected, predicted))


# In[17]:

# train model
model = SVC(kernel='linear')
expected[0:10]
model = model.fit(train_data, expected[0:10])


# In[22]:

# predict
# print model.predict(train_data[0:10])
# print expected[0:10]


# In[89]:

# save results
f = open('sol.csv','w')
f.write('ID,y')
f.write('\n')
for i in range(len(predicted)):
    out = str(i) + "," + str(predicted[i]) + "\n"
    f.write(out)

